<?php

/**
 * @file
 * Expose the external feature to hostmaster.
 */

/**
 * Implements hook_hosting_feature().
 *
 * Register the external hosting feature with Aegir, initially this feature
 * will be disabled.
 */
function hosting_external_hosting_feature() {
  $modules = system_rebuild_module_data();
  $features['external'] = array(
    'title' => t('External certificate service'),
    'description' => $modules['hosting_external']->info['description'],
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_external',
    'group' => 'experimental'
    );
  return $features;
}
