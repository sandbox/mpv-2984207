<?php

/**
 * @file
 *   An external implementation of the Certificate service for the Provision API.
 */

/**
 * Implements hook_drush_init().
 */
function external_drush_init() {
  external_provision_register_autoload();
}

/**
 * Register our directory as a place to find provision classes.
 */
function external_provision_register_autoload() {
  static $loaded = FALSE;
  if (!$loaded) {
    $loaded = TRUE;
    provision_autoload_register_prefix('Provision_', dirname(__FILE__));
  }
}

/**
 * Implements hook_provision_services().
 *
 * Ensure our classes are loaded early enough to instantiate the Provision
 * services.
 */
function external_provision_services() {
  external_provision_register_autoload();
}

