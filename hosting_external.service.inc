<?php

/**
 * @file
 * External service implementation of the Certificate service type for the hosting front end.
 */

/**
 * An implementation of the certificate service type, registered with hook_hosting_service.
 */
class hostingService_Certificate_External extends hostingService_Certificate {

  public $type = 'External';
  public $name = 'External certificate';

  protected $external_certificate_path = 'default';

  /**
   * node operations
   */

  /**
   * Load associated values for the service.
   *
   * In this certificate we will use the variable system to retrieve values.
   */
  function load() {
    parent::load();
    $this->external_certificate_path = variable_get('hosting_external_certificate_path_' . $this->server->nid, $this->external_certificate_path);
  }

  /**
   * Display settings on the server node page.
   *
   * @param
   *   A reference to the associative array of the subsection of the page
   *   reserved for this service implementation.
   */
  function view(&$render) {
    parent::view($render);
    $render['external_certificate_path'] = array(
      '#type' => 'item',
      '#title' => t('External certificate path'),
      // Remember to pass the display through filter_xss!
      '#markup' => filter_xss($this->external_certificate_path),
    );
  }

  /**
   * Extend the server node form.
   *
   * @param
   *   A reference to the associative array of the subsection of the form
   *   reserved for this service implementation.
   */
  function form(&$form) {
    parent::form($form);

    $form['external_certificate_path'] = array(
      '#type' => 'textfield',
      '#title' => t('External certificate path'),
      '#description' => t("The path for the certificate files in the server (aegir will not sync this files, they must exist in the remote server). Put each certificate under a directory for the site, or use wildcard if it's a wildcard certificate. The certificate in wildcard will be used if no specific key is found."),
      '#size' => 40,
      '#default_value' => $this->external_certificate_path,
      '#maxlength' => 64,
      '#weight' => 5,
    );
  }

  /**
   * Validate a form submission.
   */
  function validate(&$node, &$form, &$form_state) {
    parent::validate($node, $form, $form_state);

    // TODO: Validate files exist? This may be a remote server.
  }

  /**
   * Insert a record into the database.
   */
  function insert() {
    parent::insert();

    variable_set('hosting_external_certificate_path_' . $this->server->nid, $this->external_certificate_path);
  }

  /**
   * Update a record in the database.
   */
  function update() {
    parent::update();

    variable_set('hosting_external_certificate_path_' . $this->server->nid, $this->external_certificate_path);
  }

  /**
   * Delete a record from the database, based on server node.
   */
  function delete() {
    parent::delete();

    variable_del('hosting_external_certificate_path_' . $this->server->nid);
  }

  /**
   * Delete a specific revision from the database.
   */
  function delete_revision() {
    parent::delete_revision();
  }

  /**
   * Pass values to the provision backend when we call provision-save.
   */
  public function context_options($task_type, $ref_type, &$task) {
    parent::context_options($task_type, $ref_type, $task);

    $task->context_options['external_certificate_path'] = $this->external_certificate_path;
  }
}
